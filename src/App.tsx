import React, { FC, ChangeEvent, useState} from "react";
import "./app.css";
import {ITask} from "./Interfaces";

const App: FC = () => {
  const [task, setTask] = useState<string>("");
  const [deadline, setDeadline] = useState<number>(0);
  const [todoList, setTodoList] = useState<ITask[]>([]);

  const handleChange = (event: ChangeEvent<HTMLInputElement>):void => {
    if (event.target.name === "task") {
      setTask(event.target.value);
    } else {
      setDeadline(Number(event.target.value));
    }
  }

  const addTask = ():void => {
    const newTask = {
      taskName: task,
      deadLine: deadline
    };
    setTodoList([...todoList,newTask]);
  }

  return (
    <div className="app">
      <div className="header">
        <div className="inputContainer">
          <input type="text" placeholder="Task..." name="task" onChange={handleChange} />
          <input type="number" placeholder="Dealine" name="deadline" onChange={handleChange} />
        </div>
        <button onClick={addTask}> Add task</button>
      </div>
      <div className="todoList"></div>
    </div>
  );
};

export default App;
